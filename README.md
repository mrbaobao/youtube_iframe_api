# youtube_iframe_api
- This package is a wrapper for youtube iframe api
- From youtube: ```https://developers.google.com/youtube/iframe_api_reference``` [**link**](https://developers.google.com/youtube/iframe_api_reference)
# Example
1. Enable Youtube iframe api
- Function: ```enable_youtube_iframe_api```({```src```, ```origin```}): string
- **MUST HAVE**: We must add ```enablejsapi``` and ```origin``` params by this function, the ```youtube iframe api``` will be able to work properly

```
const src = "https://youtu.be/PDCiOpxCrpw?si=ubXxpnlnAQF6N0gD"
src = enable_youtube_iframe_api({src, origin: "http://example.com:3000"}) //<-- src + "&enablejsapi=1&origin=" + origin
```
2. Load youtube iframe api
- Function: ```load_youtube_iframe_api```(): ```Promise<any>```
- Return: ```window.YT```
```
const YT = await load_youtube_iframe_api() //<-- YT = window.YT
```
