/**
 * 1. Input
 * - src: Youtube video src
 * - origin: root origin of window.location
 * 2. Return: src + "&enablejsapi=1&origin=" + origin
 * 3. Example
 * - src = enable_youtube_iframe_api({src, origin: "http://example.com:3000"})
 * @param param0 
 * @returns 
 */
const enable_youtube_iframe_api = ({ src, origin }: { src: string, origin: string }): string => {
    //Check src
    if (!src || typeof src !== "string") {
        throw { error: "Param src is not valid", src }
    }
    //Check origin
    if (!origin || typeof origin !== "string") {
        throw { error: "Param origin is not valid", origin }
    }
    //Check ?
    const andKey = src.indexOf("?") >= 0 ? "&" : "?"
    ///Enable iframe Api
    src += andKey + "enablejsapi=1"
    //Set origin
    src += "&origin=" + origin
    ///Return src with enablejsapi and origin params
    return src
}
///Export
export { enable_youtube_iframe_api }