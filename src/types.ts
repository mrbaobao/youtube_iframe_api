/**
 * State
 */
enum PlayerStates {
    BUFFERING = 3,
    CUED = 5,
    ENDED = 0,
    PAUSED = 2,
    PLAYING = 1,
    UNSTARTED = -1
}
/**
 * YT Type
 */
type IframeApiType = {
    Player: any,
    PlayerState: PlayerStates,
}
/**
 * Player
 */
type PlayerType = {
    playVideo: Function
    stopVideo: Function
}
///Export
export { PlayerStates }
export type { IframeApiType, PlayerType }