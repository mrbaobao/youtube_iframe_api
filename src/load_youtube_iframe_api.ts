import { IframeApiType } from "./types";
/**
 * Example: 
 * ```
 * const YT = await load_youtube_iframe_api()
 * ```
 * @returns window.YT from Youtube iframe api
 */
const load_youtube_iframe_api = (): Promise<IframeApiType> => {
  /**
   * Check YT loaded
   */
  if (window["YT"]) {
    console.log("YT did load = ", window["YT"])
    return window["YT"]
  }
  /**
   * Keys
   */
  const youtubeIframeApiSrc = 'https://www.youtube.com/iframe_api';
  const onYouTubeIframeAPIReady = "onYouTubeIframeAPIReady"
  /**
   * Youtube Player
   */
  let tag = document.createElement('script');
  tag.src = youtubeIframeApiSrc
  let firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode?.insertBefore(tag, firstScriptTag);

  return new Promise((resolve, reject) => {
    const handler = window[onYouTubeIframeAPIReady];
    window[onYouTubeIframeAPIReady] = () => {
      if (handler) {
        handler();
      }
      resolve(window["YT"]);
    };
  });
};
export { load_youtube_iframe_api }